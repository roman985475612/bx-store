<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Интернет-магазин "Тестовый"');

$APPLICATION->IncludeComponent(
    'bitrix:catalog.section',
    'catalog_on_main',
    [
        'IBLOCK_TYPE'   => 'catalog',
        'IBLOCK_ID'     => get_iblock_id_by_code('catalog'),
    ],
    false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>