<?php
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php";

$APPLICATION->SetPageProperty('title', 'Мои заказы');
$APPLICATION->SetTitle('Мои заказы');

echo $APPLICATION->GetTitle();

$APPLICATION->IncludeComponent(
    "bitrix:sale.personal.order",
    "",
    Array(
        "STATUS_COLOR_N" => "green",
        "STATUS_COLOR_P" => "yellow",
        "STATUS_COLOR_F" => "gray",
        "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
        "SEF_MODE" => "Y",
        "ORDERS_PER_PAGE" => 20,
        "PATH_TO_PAYMENT" => "payment.php",
        "PATH_TO_BASKET" => "basket.php",
        "SET_TITLE" => "Y",
        "SAVE_IN_SESSION" => "Y",
        "NAV_TEMPLATE" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "PROP_1" => Array(),
        "PROP_2" => Array(),
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "Y",
        "CUSTOM_SELECT_PROPS" => "",
        "HISTORIC_STATUSES" => "F",
        "SEF_FOLDER" => "/",
        "SEF_URL_TEMPLATES" => Array(
            "list" => "index.php",
            "detail" => "order_detail.php?ID=#ID#",
            "cancel" => "order_cancel.php?ID=#ID#"
        ),
        "VARIABLE_ALIASES" => Array(
            "list" => Array(),
            "detail" => Array(
                "ID" => "ID"
            ),
            "cancel" => Array(
                "ID" => "ID"
            ),
        )
    ),
);

require $_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php";