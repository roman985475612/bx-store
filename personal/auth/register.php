<?php
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php";

$APPLICATION->SetPageProperty('title', 'Регистрация');
$APPLICATION->SetTitle('Регистрация');

echo $APPLICATION->GetTitle();

$APPLICATION->IncludeComponent(
    'bitrix:main.register',
    '',
    [
        'SHOW_FIELDS'       => ['EMAIL'],
        'REQUIRED_FIELDS'   => ['EMAIL'],
        'AUTH'              => 'Y',
        'SUCCESS_PAGE'      => '/personal/'
    ]
);

require $_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php";