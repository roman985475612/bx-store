<?php
define ("NEED_AUTH", true);
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php";

$APPLICATION->SetPageProperty('title', 'Восстановление пароля');
$APPLICATION->SetTitle('Восстановление пароля');

echo $APPLICATION->GetTitle();

if ( !CUser::isAuthorized() ) {
    $APPLICATION->IncludeComponent(
        'bitrix:system.auth.forgotpasswd',
        '',
        [
            'SHOW_ERRORS' => 'Y'
        ]
    );    
}

require $_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php";