<?php
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php";

$APPLICATION->SetPageProperty('title', 'Авторизация');
$APPLICATION->SetTitle('Авторизация');

echo $APPLICATION->GetTitle();

$APPLICATION->IncludeComponent(
    'bitrix:system.auth.form',
    '',
    [
        'REGISTER_URL'          => 'register.php',
        'FORGOT_PASSWORD_URL'   => 'forgotpasswd.php',
        'PROFILE_URL'           => '/personal/',
        'SHOW_ERRORS'           => 'Y'
    ]
);

require $_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php";