<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!doctype html>
<html>
<head>
    <?php
    $APPLICATION->ShowHead();

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/style.css');

    CJSCore::Init(array("jquery"));
    Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/main.js');

    Asset::getInstance()->addString("<link rel='shortcut icon' href='/local/favicon.ico' />");
    Asset::getInstance()->addString("<meta name='viewport' content='width=device-width, initial-scale=1'>");
    Asset::getInstance()->addString("<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext' rel='stylesheet'>");
    Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">');
    ?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<?$APPLICATION->ShowPanel();?>

<header class="page-header">
    <?php $APPLICATION->IncludeComponent('bitrix:menu', 'top_menu', [
        'ROOT_MENU_TYPE'        => 'top',
        'MAX_LEVEL'             => '2',
        'CHILD_MENU_TYPE'       => 'section',
        'USE_EXT'               => 'Y',
        'DELAY'                 => 'N',
        'ALLOW_MULTI_SELECT'    => 'Y',
        'MENU_CACHE_TYPE'       => 'N',
        'MENU_CACHE_TIME'       => '3600',
        'MENU_CACHE_USE_GROUPS' => 'Y',
        'MENU_CACHE_GET_VARS'   => ''
    ]);?>
</header>

<div class="container">
    <div class="row">
        <div class="col s3">
            <aside>
                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'catalog_menu', [
                    'ROOT_MENU_TYPE'        => 'catalog',
                    'MAX_LEVEL'             => '2',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);?>
            </aside>
        </div>
        <div class="col s9">
            <main>