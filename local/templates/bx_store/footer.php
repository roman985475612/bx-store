<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
            </main>
        </div>
    </div>
</div>

<footer class="page-footer cyan lighten-1">
    <div class="container">
        <div class="row">
            <div class="col l3 s12">
                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'bottom_menu', [
                    'ROOT_MENU_TYPE'        => 'bottom1',
                    'MAX_LEVEL'             => '1',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);?>
            </div>
            <div class="col l3 s12">
                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'bottom_menu', [
                    'ROOT_MENU_TYPE'        => 'bottom2',
                    'MAX_LEVEL'             => '1',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);?>
            </div>
            <div class="col l3 s12">
                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'bottom_menu', [
                    'ROOT_MENU_TYPE'        => 'bottom3',
                    'MAX_LEVEL'             => '1',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);?>
            </div>
            <div class="col l3 s12">
                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'bottom_menu', [
                    'ROOT_MENU_TYPE'        => 'bottom4',
                    'MAX_LEVEL'             => '1',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);?>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>

</body>
</html>