<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="row">
    <?php foreach($arResult as $item): ?>
    <div class="col s4">
        <div class="card medium">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="<?= $item['photo'] ?>">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4"><?= $item['title'] ?><i class="material-icons right">more_vert</i></span>
                <p><a href="<?= $item['link'] ?>">Show more</a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4"><?= $item['title'] ?><i class="material-icons right">close</i></span>
                <p><?= $item['text'] ?></p>
            </div>
        </div>
    </div>
    <?php endforeach ?>
</div>

