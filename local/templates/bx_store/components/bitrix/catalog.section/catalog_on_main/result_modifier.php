<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$cards = [];
foreach($arResult['ITEMS'] as $item) {
    $cards[] = [
        'title' => $item['NAME'],
        'link'  => $item['DETAIL_PAGE_URL'],
        'photo' => $item['PREVIEW_PICTURE']['SRC'],
        'text'  => $item['PREVIEW_TEXT'],
    ];
}
$arResult = $cards;