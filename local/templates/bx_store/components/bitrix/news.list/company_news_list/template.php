<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="row">
    <?php foreach ($arResult['ITEMS'] as $item): ?>
        <div class="col s12 m12">
            <div class="card">
                <div class="card-image">
                    <img src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>">
                    <span class="card-title"><?= $item['NAME'] ?></span>
                </div>
                <div class="card-content"><?= $item['PREVIEW_TEXT'] ?></div>
                <div class="card-action">
                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>">Read more</a>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <div class="col s12 m12">
        <?= $arResult["NAV_STRING"] ?>
    </div>
</div>
