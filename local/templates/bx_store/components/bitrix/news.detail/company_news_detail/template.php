<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//dd($arResult);
?>
<div class="row">
    <div class="col s12 m12">
        <div class="card">
            <div class="card-image">
                <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
                <span class="card-title"><?= $arResult['NAME'] ?></span>
            </div>
            <div class="card-content"><?= $arResult['DETAIL_TEXT'] ?></div>
            <div class="card-action">
                <a href="<?= $arResult['LIST_PAGE_URL'] ?>">Back to list</a>
            </div>
        </div>
    </div>
</div>
