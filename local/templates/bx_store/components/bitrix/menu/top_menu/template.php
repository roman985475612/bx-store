<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<!-- Dropdown Structure -->
<?php foreach ($arResult as $key => $item): ?>
    <?php if ($item['IS_PARENT'] == 1): ?>
        <ul id="dropdown<?= $key ?>" class="dropdown-content">
            <li><a href="<?=$item['LINK']?>"><?=$item['TEXT']?></a></li>
            <li class="divider"></li>
            <?php foreach ($item['subitems'] as $subitem): ?>
                <li><a href="<?=$subitem['LINK']?>"><?=$subitem['TEXT']?></a></li>
            <?php endforeach ?>
        </ul>
    <?php endif ?>
<?php endforeach ?>
<nav class="cyan lighten-1">
    <div class="container">
        <div class="nav-wrapper">
            <a href="/" class="brand-logo">City</a>
            <ul class="right hide-on-med-and-down">
                <?php foreach ($arResult as $key => $item): ?>
                    <?php if ($item['DEPTH_LEVEL'] == 1): ?>
                        <?php if ($item['IS_PARENT'] == 1): ?>
                            <li><a class="dropdown-trigger" href="#1" data-target="dropdown<?=$key?>"><?=$item['TEXT']?><i class="material-icons right">arrow_drop_down</i></a></li>
                        <?php else: ?>
                            <li><a href="<?=$item['LINK']?>"><?=$item['TEXT']?></a></li>
                        <?php endif ?>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</nav>


