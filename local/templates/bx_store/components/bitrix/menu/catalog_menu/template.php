<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//dd($arResult);
?>

<ul id="catalog-menu" class="collapsible">
    <?php foreach ($arResult as $item): ?>
        <li
            <?php if ($item['SELECTED'] == 1): ?>
                class="active"
            <?php endif ?>
        >
            <?php if ($item['IS_PARENT'] == 1): ?>
                <a class="collapsible-header cyan-text text-lighten-1"><i class="material-icons">expand_more</i><?= $item['TEXT'] ?></a>
                <div class="collapsible-body">
                    <div class="collection">
                        <?php foreach($item['subitems'] as $subitems): ?>
                            <a
                                <?php
                                    $classes = ['collection-item'];
                                    if($subitems['SELECTED'] == 1) {
                                        $classes[] = 'active';
                                        $classes[] = 'cyan';
                                        $classes[] = 'lighten-1';
                                    } else {
                                        $classes[] = 'cyan-text';
                                        $classes[] = 'text-lighten-1';
                                    }
                                    $classList = implode(' ', $classes);
                                ?>
                                class="<?= $classList ?>"
                                href="<?= $subitems['LINK'] ?>"
                            >
                                <i class="material-icons">local_offer</i><?= $subitems['TEXT'] ?>
                            </a>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php else: ?>
                <a
                    <?php
                        $classes = ['collapsible-header'];
                        if($item['SELECTED'] == 1) {
                            $classes[] = 'active';
                            $classes[] = 'cyan';
                            $classes[] = 'lighten-1';
                            $classes[] = 'white-text';
                        } else {
                            $classes[] = 'cyan-text';
                            $classes[] = 'text-lighten-1';
                        }
                        $classList = implode(' ', $classes);
                    ?>
                    class="<?= $classList ?>"
                    href="<?= $item['LINK'] ?>"
                >
                    <i class="material-icons">local_offer</i><?= $item['TEXT'] ?>
                </a>
            <?php endif ?>
        </li>
    <?php endforeach ?>
</ul>
