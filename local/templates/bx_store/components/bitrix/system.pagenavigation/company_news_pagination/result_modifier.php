<?php

$queryString = $arResult['sUrlPathParams'].'PAGEN_'.$arResult['NavNum'].'=';

$prev = $arResult['NavPageNomer'] - 1;
$next = $arResult['NavPageNomer'] + 1;

if ($prev > 1)
    $prevQueryString = $queryString . $prev;
elseif ($prev = 1)
    $prevQueryString = $arResult['sUrlPath'];

if ($next <= $arResult['NavPageCount'])
    $nextQueryString = $queryString . $next;

$pages = [];

if ($arResult['NavPageNomer'] > 1) {
    $pages['prev'] = [
        'class' => 'waves-effect',
        'link'  => $prevQueryString,
    ];

} else {
    $pages['prev'] = [
        'class' =>  'disabled',
        'link'  => '',
    ];
}
$pages['prev']['text'] = '<i class="material-icons">chevron_left</i>';

for ($i = 1; $i <= $arResult['NavPageCount']; $i++) {
    if ($i == $arResult['NavPageNomer']) {
        $page = [
            'class' => 'active',
            'link'  => '#!',
        ];
    } else if ($i == 1) {
        $page = [
            'class' => 'waves-effect',
            'link'  => $prevQueryString,
        ];
    } else {
        $page = [
            'class' => 'waves-effect',
            'link'  => $queryString . $i,
        ];
    }

    $page['text'] = $i;

    $pages[$i] = $page;
}

if ($arResult['NavPageNomer'] < $arResult['NavPageCount']) {
    $pages['next'] = [
        'class' => 'waves-effect',
        'link'  => $nextQueryString,
    ];

} else {
    $pages['next'] = [
        'class' =>  'disabled',
        'link'  => '',
    ];
}
$pages['next']['text'] = '<i class="material-icons">chevron_right</i>';

$arResult['pages'] = $pages;
