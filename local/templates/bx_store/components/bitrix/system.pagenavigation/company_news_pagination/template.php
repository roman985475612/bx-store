<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if ($arResult['NavPageCount'] < 2)
    return;
?>

<ul class="pagination">
    <?php foreach ($arResult['pages'] as $page): ?>
        <li class="<?= $page['class'] ?>"><a href="<?= $page['link'] ?>"><?= $page['text'] ?></a></li>
    <?php endforeach ?>
</ul>
