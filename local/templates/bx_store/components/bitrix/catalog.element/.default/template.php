<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div id="product-detail" class="row">
    <div class="col s7">
        <img class="responsive-img" src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
        <p class="flow-text"><?= $arResult['DETAIL_TEXT'] ?></p>
    </div>
    <div class="col s5">
        <h1><?= $arResult['NAME'] ?></h1>
        <table class="responsive-table">
            <tbody>
                <tr>
                    <th>Цена</th>
                    <td><?= $arResult['CATALOG_PURCHASING_PRICE'] . ' ' . $arResult['CATALOG_PURCHASING_CURRENCY'] ?></td>
                </tr>
                <?php foreach($arResult['PROPERTIES'] as $property): ?>
                    <tr>
                        <th><?= $property['NAME'] ?></th>
                        <td><?= $property['VALUE'] ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
