<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty('title', 'Новости');
$APPLICATION->SetTitle('Новости');

$APPLICATION->IncludeComponent(
    'bitrix:news',
    'company_news',
    [
        'IBLOCK_TYPE'               => 'content',
        'IBLOCK_ID'                 => get_iblock_id_by_code('news'),
        'NEWS_COUNT'                => 1,
        'PAGER_TEMPLATE'            => 'company_news_pagination',
        'SEF_MODE'                  => 'Y',
        'SEF_FOLDER'                => '/news/',
        'SEF_URL_TEMPLATES'         => [
            'detail' => '#ELEMENT_CODE#/',
        ],
        'BROWSER_TITLE'             => 'NAME',
        'LIST_PROPERTY_CODE'        => ['SOURCE', 'MORE_PHOTO'],
        'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
        'SET_TITLE'                 => 'Y',
        'SET_STATUS_404'            => 'Y',
        'SHOW_404'                  => 'Y',
    ],
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");