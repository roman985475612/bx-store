<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty('title', 'Каталог товаров');
$APPLICATION->SetTitle('Каталог товаров');

$APPLICATION->IncludeComponent(
    'bitrix:catalog',
    '',
    [
        'IBLOCK_TYPE'       => 'catalog',
        'IBLOCK_ID'         => get_iblock_id_by_code('catalog'),
        'ADD_PICT_PROP'     => 'MORE_PHOTO',
        'SEF_MODE'          => 'Y',
        'SEF_FOLDER'        => '/catalog/',
        'SEF_URL_TEMPLATES' => [
            'sections'  => '',
            'section'   => '#SECTION_CODE_PATH#/',
            'element'   => '#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
            'compare'   => 'compare/'
        ],
        'SET_TITLE'         => 'Y',
    ],
    false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");