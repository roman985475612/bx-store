<?php
$arUrlRewrite = [
    0  => [
        'CONDITION' => '#^/news/#',
        'RULE'      => '',
        'ID'        => 'bitrix:news',
        'PATH'      => '/news/index.php',
        'SORT'      => 100,
    ],
    1  => [
        'CONDITION' => '#^/catalog/#',
        'RULE'      => '',
        'ID'        => 'bitrix:catalog',
        'PATH'      => '/catalog/index.php',
        'SORT'      => 100,
    ],
];
