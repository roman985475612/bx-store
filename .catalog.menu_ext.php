<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt = $APPLICATION->IncludeComponent('bitrix:menu.sections', '', [
    'IBLOCK_TYPE'       => 'catalog',
    'IBLOCK_ID'         => get_iblock_id_by_code('catalog'),
    'IS_SEF'            => 'Y',
    'SEF_BASE_URL'      => '/catalog/',
    'SECTION_PAGE_URL'  => '#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
    'DEPTH_LEVEL'       => '3',
    'CACHE_TYPE'        => 'A',
    'CACHE_TIME'        => '36000000'
]);

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);

